## code to prepare `metadata` dataset goes here
library(dplyr)
library(vroom)
library(forcats)

metadata <- vroom("C:/Data/git/hackathon_datascience_2021/data/RNA-Seq_datasets_USER_2007.txt",
  col_types = cols(
    .default = col_factor(),
    Paper = col_character(),
    Read_length_bp = col_integer(),
    Short_stage = col_character(),
    Detailed_stage = col_character(),
    # Added_date = col_factor(), #col_character(),
    Treatment_detail = col_character(),
    Comments = col_character(),
  ),
  na = "-"
) %>%
  mutate(Public = `Private_public?` == "Public", .after = "File_name", .keep = "unused") %>%
  mutate(PairedEnd = PE_SE == "PE", .after = "Sequencing_platform", .keep = "unused") %>%
  mutate(Organ = fct_collapse(Organ,
    Inflorescence = c("Inflorescence", "inflorescence"), # il y a des différences de majuscule/minuscule entre même organes
    Flower = c("Flower", "Buds", "Flower buds") # on change les organes flower buds et buds en flower
  )) %>%
  # mutate(Stranded = Stranded == "reverse") %>%
  # mutate(Added_date = parse_date(Added_date, format = ifelse(grepl("-", Added_date), "%Y-%m", "%Y"))) %>%
  rename(Treatment = `Treatment?`) %>%
  mutate(Complete_Bioproject = Complete_Bioproject == "Yes")

#usethis::use_data(metadata, overwrite = TRUE, compress = 'gzip')
