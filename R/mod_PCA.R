#' PCA UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @importFrom dplyr filter pull mutate_if
#' @importFrom forcats fct_drop
#' @importFrom stats prcomp
#' @importFrom broom augment
#' @importFrom ggplot2 ggplot aes_string geom_point stat_ellipse
#' @importFrom rlang .data
#' @importFrom shinydashboardPlus box boxSidebar
#' @importFrom shinycssloaders withSpinner
#' 
#' @noRd 
#'
#' @importFrom shiny NS tagList 
mod_PCA_ui <- function(id, metadataset) {
  ns <- NS(id)
  tagList(
    box(title = "Principal Component Analysis",
        width = NULL,
        sidebar = boxSidebar(
          id = "PCA_settings",
          width = 25,
          startOpen = TRUE,
          varSelectInput(ns("color"), "Color by:",
                         data = metadataset, selected = "Organ"),
          checkboxInput(ns("ellipse"), "Add ellipse"),
          varSelectInput(ns("shape"), "Shape by:",
                         data = metadataset, selected = "Sequencing_platform")
          ),
        withSpinner(plotOutput(ns("plot"),
                               height = "600px"))
        )
    )
}
    
#' PCA Server Functions
#'
#' @noRd 
mod_PCA_server <- function(id, dataset, metadataset){
  moduleServer( id, function(input, output, session){
    ns <- session$ns
    
    pca <- reactive({
      m <- metadataset %>% 
      filter(.data$File_name %in% row.names(dataset)) %>% 
      mutate_if(is.factor, fct_drop)
      
    prcomp(dataset, scale. = TRUE) %>% 
      augment(m)
    })
    
    output$plot <- renderPlot({
      p <- ggplot(data = pca(),
             aes_string(x = ".fittedPC1", y = ".fittedPC2", 
                        color = input$color, shape = input$shape)) + 
        geom_point()
      if (input$ellipse) {
        p <- p + stat_ellipse()
      }
      return(p)
    })
  })
}
    
## To be copied in the UI
# mod_PCA_ui("PCA_1")
    
## To be copied in the server
# mod_PCA_server("PCA_1")
