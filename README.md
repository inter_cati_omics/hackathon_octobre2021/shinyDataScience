
<!-- README.md is generated from README.Rmd. Please edit that file -->

# shinyDataScience

<!-- badges: start -->
<!-- badges: end -->

## Installation

``` r
remotes::install_gitlab("inter_cati_omics/hackathon_octobre2021/shinyDataScience", host = "forgemia.inra.fr", auth_token = "9gY9RGQ4ktg_TGn339xT")
```

## runApp

``` r
shinyDataScience::run_app()
```

## Docker

``` bash
docker pull cmidoux/hds:2021-09-13
```

## Shinyapps

-   <https://migale.shinyapps.io/shinyDataSciences/>
